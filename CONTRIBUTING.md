This guide is for anyone who wants to publish a desktop app on the Lunni
Marketplace. It is primarily intended for app authors, however we welcome
submissions from anybody.


## What Lunni Marketplace offers

Lunni is a Docker Swarm GUI. It helps startups, small businesses, and
self-hosting enthusiasts deploy and manage apps on their own server.

Lunni Marketplace is a centralized repository of apps that you can host on a
Docker Swarm (via Lunni or manually). It provides a single point of entry for
distributing apps on the most popular server Linux distributions.

Once an app is included in Lunni Marketplace, it can be easily installed by
users from their Lunni dashboard, by searching for the app in the Marketplace
section.

Lunni also provides a means for app updates to be distributed to users – once
an app is included, its developers can make new releases, and users can install
them in one click.

Finally, Lunni can help with exposure for applications, through the Marketplace
section where available apps are advertised, and through our own websites and
social media channels.


## Who can use Lunni Marketplace

Lunni Marketplace is primarily intended for use by developers who want to
distribute their server apps on Linux. Apps must have a public Docker image
published (not necessarily on Docker Hub, any public repository will do).

If you are responsible for a commercial app that you would like to distribute
through Lunni Marketplace, you are also welcome to submit it. Contact us to work it out:
<lunni@aedge.dev>


## How to submit an app

TODO


## Someone else has put my app on Lunni Marketplace – what do I do?

Lunni Marketplace is primarily intended as a service that is used by app
developers to distribute their apps. Our goal is to give developers control of
their apps and to allow them a closer relationship with their users without
middlemen getting in the way. However, as part of setting up the Marketplace,
some applications are being distributed without the involvement of their
developers. We would prefer that these applications are controlled by their
authors.

If an application that belongs to you is being distributed without your
involvement, please [get in touch](mailto:lunni@aedge.dev), so that we can
discuss transfering ownership.
