version: "3.8"

x-defaults:
  base: &base
    image: chatwoot/chatwoot:latest-ce
    volumes:
      - chatwoot_data:/app/storage
    environment:
      # Adapted from https://raw.githubusercontent.com/chatwoot/chatwoot/develop/.env.example

      NODE_ENV: "production"
      RAILS_ENV: "production"
      INSTALLATION_ENV: "docker"

      SECRET_KEY_BASE: "${SECRET_KEY_BASE?}"
      FRONTEND_URL: "https://${DOMAIN?}"
      # To use a dedicated URL for help center pages:
      # HELPCENTER_URL: "http://0.0.0.0:3000"
      FORCE_SSL: "false"
      ENABLE_ACCOUNT_SIGNUP: "false"
      REDIS_URL: redis://redis:6379
      POSTGRES_HOST: "postgres"
      POSTGRES_USERNAME: "postgres"
      POSTGRES_PASSWORD: "12345678"
      RAILS_MAX_THREADS: "5"

      # System emails
      MAILER_SENDER_EMAIL: "${MAILER_SENDER_EMAIL:-Chatwoot <noreply@${DOMAIN?}>}"
      SMTP_ADDRESS: "${SMTP_ADDRESS?}"
      SMTP_PORT: "${SMTP_PORT?}"
      SMTP_TLS: "${SMTP_TLS:-true}"
      SMTP_USERNAME: "${SMTP_USERNAME?}"
      SMTP_PASSWORD: "${SMTP_PASSWORD?}"
      
      # Storage
      ACTIVE_STORAGE_SERVICE: "local"
      # ACTIVE_STORAGE_SERVICE: "s3_compatible"
      # STORAGE_BUCKET_NAME: "chatwoot"
      # STORAGE_ACCESS_KEY_ID: "AKXXEXAMPLEXXEXAMPLE"
      # STORAGE_SECRET_ACCESS_KEY: "internal-only"
      # STORAGE_REGION: "sa-east-1"
      # STORAGE_ENDPOINT: "http://minio:9000"

      # Logging
      RAILS_LOG_TO_STDOUT: "true"
      LOG_LEVEL: "info"
      LOG_SIZE: "500"

      # Mobile apps
      IOS_APP_ID: "L7YLMN4634.com.chatwoot.app"
      ANDROID_BUNDLE_ID: "com.chatwoot.app"
      ENABLE_PUSH_RELAY_SERVER: "true"

services:
  rails:
    <<: *base
    networks: [default, traefik-public]
    deploy:
      labels:
        - traefik.enable=true
        - traefik.docker.network=traefik-public
        - traefik.constraint-label=traefik-public
        - traefik.http.routers.${PROJECT_NAME?}-http.rule=Host(`${DOMAIN?}`)
        - traefik.http.routers.${PROJECT_NAME?}-http.entrypoints=http
        - traefik.http.routers.${PROJECT_NAME?}-http.middlewares=https-redirect
        - traefik.http.routers.${PROJECT_NAME?}-https.rule=Host(`${DOMAIN?}`)
        - traefik.http.routers.${PROJECT_NAME?}-https.entrypoints=https
        - traefik.http.routers.${PROJECT_NAME?}-https.tls=true
        - traefik.http.routers.${PROJECT_NAME?}-https.tls.certresolver=le
        - traefik.http.services.${PROJECT_NAME?}.loadbalancer.server.port=3000
    entrypoint: docker/entrypoints/rails.sh
    command: ['bundle', 'exec', 'rails', 's', '-p', '3000', '-b', '0.0.0.0']

  sidekiq:
    <<: *base
    command: ['bundle', 'exec', 'sidekiq', '-C', 'config/sidekiq.yml']

  postgres:
    image: postgres:16
    restart: always
    volumes:
      - postgresql_data:/var/lib/postgresql/data
    environment:
      - POSTGRES_DB=chatwoot
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=12345678

  redis:
    image: redis:alpine
    restart: always
    command: ["sh", "-c", "redis-server"]
    volumes:
      - redis_data:/data
    networks:
      - default

  # minio:
  #   image: minio/minio:latest
  #   command: server /data --console-address ":19001"
  #   environment:
  #     MINIO_ROOT_USER: 'AKXXEXAMPLEXXEXAMPLE'
  #     MINIO_ROOT_PASSWORD: 'internal-only'
  #   # ports:
  #   #   - 19000:9000   # S3 interface
  #   #   - 19001:19001  # Minio console

networks:
  default:
    driver: overlay
  traefik-public:
    external: true

volumes:
  chatwoot_data:
  postgresql_data:
  redis_data:
