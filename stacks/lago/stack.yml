version: "3.8"

volumes:
  lago_postgres_data:
  lago_redis_data:
  lago_storage_data:

networks:
  default:
  traefik-public:
    external: true

services:
  db:
    image: postgres:14.12-alpine
    networks: [default]
    environment:
      POSTGRES_DB: lago
      POSTGRES_USER: ${POSTGRES_USER:-lago}
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD:-changeme}
      PGDATA: /data/postgres
    volumes:
      - lago_postgres_data:/data/postgres

  redis:
    image: redis:6.2-alpine
    networks: [default]
    volumes:
      - lago_redis_data:/data

  api:
    image: getlago/api:${LAGO_VERSION:-v0.27.0-beta}
    command: ["./scripts/start.sh"]
    environment:
      - LAGO_API_URL=https://api.${DOMAIN?}
      - DATABASE_URL=postgresql://${POSTGRES_USER:-lago}:${POSTGRES_PASSWORD:-changeme}@db:5432/lago
      - REDIS_URL=redis://redis:6379
      - REDIS_PASSWORD=${REDIS_PASSWORD}
      - SECRET_KEY_BASE=${SECRET_KEY_BASE?}
      - RAILS_ENV=production
      - RAILS_LOG_TO_STDOUT=true
      - SENTRY_DSN=${SENTRY_DSN:-}
      - LAGO_FRONT_URL=https://${DOMAIN?}
      - RSA_PRIVATE_KEY=${LAGO_RSA_PRIVATE_KEY?}  # Should be base64 encoded
      - LAGO_RSA_PRIVATE_KEY=${LAGO_RSA_PRIVATE_KEY?}  # Should be base64 encoded
      - LAGO_SIDEKIQ_WEB=${LAGO_SIDEKIQ_WEB:-}
      - ENCRYPTION_PRIMARY_KEY=${LAGO_ENCRYPTION_PRIMARY_KEY?}
      - ENCRYPTION_DETERMINISTIC_KEY=${LAGO_ENCRYPTION_DETERMINISTIC_KEY?}
      - ENCRYPTION_KEY_DERIVATION_SALT=${LAGO_ENCRYPTION_KEY_DERIVATION_SALT?}
      - LAGO_USE_AWS_S3=false
      - LAGO_PDF_URL=http://pdf:3000
      - LAGO_REDIS_CACHE_URL=redis://redis:6379
      - LAGO_REDIS_CACHE_PASSWORD=${LAGO_REDIS_CACHE_PASSWORD}
      - LAGO_DISABLE_SEGMENT=true
      - LAGO_OAUTH_PROXY_URL=https://proxy.getlago.com
    volumes:
      - lago_storage_data:/app/storage
    networks: [default, traefik-public]
    deploy:
      labels:
        - traefik.enable=true
        - traefik.docker.network=traefik-public
        - traefik.constraint-label=traefik-public
        - traefik.http.routers.${PROJECT_NAME?}_api-http.rule=Host(`api.${DOMAIN?}`)
        - traefik.http.routers.${PROJECT_NAME?}_api-http.entrypoints=http
        - traefik.http.routers.${PROJECT_NAME?}_api-http.middlewares=https-redirect
        - traefik.http.routers.${PROJECT_NAME?}_api-https.rule=Host(`api.${DOMAIN?}`)
        - traefik.http.routers.${PROJECT_NAME?}_api-https.entrypoints=https
        - traefik.http.routers.${PROJECT_NAME?}_api-https.tls=true
        - traefik.http.routers.${PROJECT_NAME?}_api-https.tls.certresolver=le
        - traefik.http.services.${PROJECT_NAME?}_api.loadbalancer.server.port=3000

  front:
    image: getlago/front:${LAGO_VERSION:-v0.27.0-beta}
    environment:
      - API_URL=https://api.${DOMAIN?}
      - APP_ENV=${APP_ENV:-production}
      - CODEGEN_API=https://api.${DOMAIN?}
      - LAGO_DISABLE_SIGNUP=${LAGO_DISABLE_SIGNUP:-false}
      - LAGO_OAUTH_PROXY_URL=https://proxy.getlago.com
    networks: [default, traefik-public]
    deploy:
      labels:
        - traefik.enable=true
        - traefik.docker.network=traefik-public
        - traefik.constraint-label=traefik-public
        - traefik.http.routers.${PROJECT_NAME?}-http.rule=Host(`${DOMAIN?}`)
        - traefik.http.routers.${PROJECT_NAME?}-http.entrypoints=http
        - traefik.http.routers.${PROJECT_NAME?}-http.middlewares=https-redirect
        - traefik.http.routers.${PROJECT_NAME?}-https.rule=Host(`${DOMAIN?}`)
        - traefik.http.routers.${PROJECT_NAME?}-https.entrypoints=https
        - traefik.http.routers.${PROJECT_NAME?}-https.tls=true
        - traefik.http.routers.${PROJECT_NAME?}-https.tls.certresolver=le
        - traefik.http.services.${PROJECT_NAME?}.loadbalancer.server.port=80

  api-worker:
    image: getlago/api:${LAGO_VERSION:-v0.27.0-beta}
    depends_on:
      - api
    command: ["./scripts/start.worker.sh"]
    networks: [default]
    environment:
      - LAGO_API_URL=https://api.${DOMAIN?}
      - DATABASE_URL=postgresql://${POSTGRES_USER:-lago}:${POSTGRES_PASSWORD:-changeme}@db:5432/lago
      - REDIS_URL=redis://${REDIS_HOST:-redis}:${REDIS_PORT:-6379}
      - REDIS_PASSWORD=${REDIS_PASSWORD}
      - SECRET_KEY_BASE=${SECRET_KEY_BASE?}
      - RAILS_ENV=production
      - RAILS_LOG_TO_STDOUT=${LAGO_RAILS_STDOUT:-true}
      - SENTRY_DSN=${SENTRY_DSN}
      - LAGO_RSA_PRIVATE_KEY=${LAGO_RSA_PRIVATE_KEY} # Should be base64 encoded
      - RSA_PRIVATE_KEY=${LAGO_RSA_PRIVATE_KEY} # Should be base64 encoded
      - ENCRYPTION_PRIMARY_KEY=${LAGO_ENCRYPTION_PRIMARY_KEY:-your-encrpytion-primary-key}
      - ENCRYPTION_DETERMINISTIC_KEY=${LAGO_ENCRYPTION_DETERMINISTIC_KEY:-your-encrpytion-deterministic-key}
      - ENCRYPTION_KEY_DERIVATION_SALT=${LAGO_ENCRYPTION_KEY_DERIVATION_SALT:-your-encrpytion-derivation-salt}
      - LAGO_USE_AWS_S3=false
      - LAGO_USE_GCS=false
      - LAGO_PDF_URL=http://pdf:3000
      - LAGO_REDIS_CACHE_URL=redis://redis:6379
      - LAGO_REDIS_CACHE_PASSWORD=${LAGO_REDIS_CACHE_PASSWORD}
      - LAGO_DISABLE_SEGMENT=true
    volumes:
      - lago_storage_data:/app/storage

  api-clock:
    image: getlago/api:${LAGO_VERSION:-v0.27.0-beta}
    command: ["./scripts/start.clock.sh"]
    networks: [default]
    environment:
      - LAGO_API_URL=https://api.${DOMAIN?}
      - DATABASE_URL=postgresql://${POSTGRES_USER:-lago}:${POSTGRES_PASSWORD:-changeme}@db:5432/lago
      - REDIS_URL=redis://${REDIS_HOST:-redis}:${REDIS_PORT:-6379}
      - REDIS_PASSWORD=${REDIS_PASSWORD}
      - SECRET_KEY_BASE=${SECRET_KEY_BASE?your-secret-key-base-hex-64}
      - RAILS_ENV=production
      - RAILS_LOG_TO_STDOUT=${LAGO_RAILS_STDOUT:-true}
      - SENTRY_DSN=${SENTRY_DSN}
      - LAGO_RSA_PRIVATE_KEY=${LAGO_RSA_PRIVATE_KEY} # Should be base64 encoded
      - RSA_PRIVATE_KEY=${LAGO_RSA_PRIVATE_KEY} # Should be base64 encoded
      - ENCRYPTION_PRIMARY_KEY=${LAGO_ENCRYPTION_PRIMARY_KEY?}
      - ENCRYPTION_DETERMINISTIC_KEY=${LAGO_ENCRYPTION_DETERMINISTIC_KEY?}
      - ENCRYPTION_KEY_DERIVATION_SALT=${LAGO_ENCRYPTION_KEY_DERIVATION_SALT?}

  pdf:
    image: getlago/lago-gotenberg:7
    networks: [default]
