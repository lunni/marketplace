import json
from pathlib import Path
import sys
from traceback import print_exc


RELATIVE_URL_TEMPLATE = "https://marketplace-api.lunni.dev/{app_name}/{filename}"

output_path = Path("./public")

apps_dict = {}

for app_p in Path("./stacks").glob("*/app.json"):
    app_input_dir = app_p.parent
    app_name = app_input_dir.name

    try:
        with app_p.open() as app_fp:
            app = json.load(app_fp)

        app_output_dir = output_path / app_name
        app_output_dir.mkdir(exist_ok=True)

        # Use the banner.png as a banner
        if (app_input_dir / "banner.png").exists():
            (app_output_dir / "banner.png").hardlink_to(app_input_dir / "banner.png")
            app["meta"]["bannerUrl"] = RELATIVE_URL_TEMPLATE.format(
                app_name=app_name,
                filename="banner.png",
            )

        # Use the first file named icon.* as an app icon
        for icon_p in app_input_dir.glob("icon.*"):
            (app_output_dir / icon_p.name).hardlink_to(icon_p)
            app["meta"]["iconUrl"] = RELATIVE_URL_TEMPLATE.format(
                app_name=app_name,
                filename=icon_p.name,
            )
            break

        # Use screenshots/* folder as screenshots
        app["meta"]["screenshots"] = []
        (app_output_dir / "screenshots").mkdir(exist_ok=True)
        for screenshot_p in app_input_dir.glob("screenshots/*"):
            (app_output_dir / "screenshots" / screenshot_p.name).hardlink_to(screenshot_p)
            app["meta"]["screenshots"].append(RELATIVE_URL_TEMPLATE.format(
                app_name=app_name,
                filename=f"screenshots/{screenshot_p.name}",
            ))

        # Save full app definition to detail endpoint (/{app_name}/app.json)
        app["meta"] = {"name": app_name, **app["meta"]}
        del app["$schema"]
        with (app_output_dir / "app.json").open("w") as output_fp:
            json.dump(app, output_fp, indent=2)

        # Save short app definition to list endpoint (/apps.json)
        apps_dict[app_name] = app["meta"]
        apps_dict[app_name]["selfUrl"] = RELATIVE_URL_TEMPLATE.format(
            app_name=app_name,
            filename="app.json",
        )

    except Exception:
        print(f"When processing {app_p}:")
        print_exc()


json.dump({
    "version": "3",
    "apps": apps_dict
}, (output_path / "apps.json").open("w"), indent=2)
