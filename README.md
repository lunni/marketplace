# Lunni Marketplace <small>alpha</small>

A collection of application templates ready for deplotment on Lunni.


## Usage

The UI for marketplace is not ready yet (we're working on it!), but you can
use it from Portainer:

1. In the upper-right corner of any page, click your profile photo, then click
   **Go to Portainer**. 

2. In the left sidebar select **App Templates**. You should see a list of
   applications available for deployment.

3. Select an application you want to deploy. Fill out the Configuration
   section.

    - Make sure to set `Name` and `Project name` to the same value.

4. Click **Deploy the stack**.
